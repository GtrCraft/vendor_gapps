# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#inherit common gapps
$(call inherit-product, vendor/gapps/common-gapps.mk)

#facelock librarys
PRODUCT_COPY_FILES += \
    vendor/gapps/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
    vendor/gapps/lib/libfacenet.so:system/lib/libfacenet.so

#velvet librarys
PRODUCT_COPY_FILES += \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libbrotli.so:system/priv-app/Velvet/lib/arm/libbrotli.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libcronet.so:system/priv-app/Velvet/lib/arm/libcronet.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libframesequence.so:system/priv-app/Velvet/lib/arm/libframesequence.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libgoogle_speech_jni.so:system/priv-app/Velvet/lib/arm/libgoogle_speech_jni.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libgoogle_speech_micro_jni.so:system/priv-app/Velvet/lib/arm/libgoogle_speech_micro_jni.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libnativecrashreporter.so:system/priv-app/Velvet/lib/arm/libnativecrashreporter.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/liboffline_actions_jni.so:system/priv-app/Velvet/lib/arm/liboffline_actions_jni.so \
    vendor/gapps/priv-app/Velvet/arm/Velvet/lib/arm/libthird_party_brotli_dec_jni.so:system/priv-app/Velvet/lib/arm/libthird_party_brotli_dec_jni.so